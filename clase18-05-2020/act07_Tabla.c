#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[]){
  int i, n;
  printf(" Ingrese la tabla que desea imprimir: ");
  scanf("%d", &n);
  for(i = 0; i < 10; i++){
    printf("  %d x %d = %d\n", i+1, n, n*(i+1));
  }
  system("pause");
  return 0;
}
