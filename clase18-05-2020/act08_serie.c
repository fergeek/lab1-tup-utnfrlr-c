#include <stdio.h>
#include <stdlib.h>
int main(int argc, char const *argv[]){
  int i, n, c = 1;
  long ac = 0;
  printf(" Ingrese la cantidad de terminos para la serie 1 + 11 + 111 + ... :");
  scanf("%d", &n);
  for(i = 0; i < n; i++){
    ac=ac+c;
    c=(c*10)+1;
  }
  printf(" Res: %ld",ac);

  /// FORMA SENCILLA
  // printf(" Para %d terminos la sumatoria es: ", n);
  // for(i = 0; i < n; i++){printf("%d",i+1);}
  // printf("\n\n");
  system("pause");
  return 0;
}
