#include <stdio.h>
#include <string.h>
void decToBin(char *str, int n);
int main(int argc, char const *argv[]){
  int n; char res[9] = "";
  printf(" Ingrese numero:");
  scanf("%d", &n);
  decToBin(res, n);
  printf("\n %s\n",res);
  return 0;
}
void decToBin(char *str, int n){
  int i = 0;
  while(n!=1){
    strcat(str,(n % 2) ? "1":"0");
    n /= 2;
    i++;
  }
  strcat(str,n ? "1" : "0");
  strrev(str);
}
