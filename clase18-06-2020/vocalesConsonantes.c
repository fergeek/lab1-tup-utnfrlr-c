#include <stdio.h>
int main(int argc, char const *argv[]){
  int i = 0, v = 0, c = 0, e = 0;
  char txt[140];
  printf(" Ingrese una frase: ");
  gets(txt);
  while (txt[i] != 0){
    if((txt[i] >= 65 && txt[i] <= 122) && !(txt[i] >= 91 && txt[i] <= 96)){
      switch(txt[i]){
        case 'a': case 'A':
        case 'e': case 'E':
        case 'i': case 'I':
        case 'o': case 'O':
        case 'u': case 'U': v++; break;
        default: c++; break;
      }
    }
    else{e++;} i++;
  }
  printf(" Caracteres\n Vocales: %d\n Consonantes: %d\n Especiales: %d\n", v, c, e);
  printf(" Texto: %s\n",txt);
  return 0;
}
