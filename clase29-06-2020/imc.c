#include <stdio.h>
#include <string.h>
typedef struct {
  char nombre[50];
  float altura;
  float peso;
  float imc;
} persona;
persona loadPersona();
int main(int argc, char const *argv[]) {
  int n = 3, i; persona personas[3];
  for (i = 0; i < n; i++) {
    printf("Ingrese persona %d \n", i + 1);
    personas[i] = loadPersona();
    printf("\n IMC: %.2f\n", personas[i].imc);
  }
  return 0;
}
persona loadPersona(){
  char nombre[50]; persona persona;
  printf(" Nombre y Apellido: ");
  gets(nombre);
  strcpy(persona.nombre, nombre);
  printf(" Peso [Kg]: ");
  scanf("%f", &persona.peso);
  printf(" Altura [m]: ");
  scanf("%f", &persona.altura);
  persona.imc = (persona.peso)/(persona.altura * persona.altura);
  return persona;
}
