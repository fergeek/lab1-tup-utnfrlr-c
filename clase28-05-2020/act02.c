#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#define n 20
#define N n+1
void printArray(int *vec, char *msj);
int main(int argc, char const *argv[]){
  srand(time(0));
  int vec[N], i;
  // Cargar Vector
  for(i = 1; i < N; i++){vec[i] = rand();}
  vec[0] = i - 1;
  printArray(vec,(char*) " vec[%d] = %d\n");
  return 0;
}
void printArray(int *vec, char *msj){
  int i;
  for(i = 1; i <= vec[0];i++){
    printf(msj, i, vec[i]);
  }
}
