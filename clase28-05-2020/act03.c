#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define n 20
#define N n+1
void printArray(int *vec, char *msj);
void loadRand(int *vec, int limit);
int * counter(int vec[]);
int main(int argc, char const *argv[]){
  srand(time(0));
  int vec[N], *ret;
  loadRand(vec, n);
  ret = counter(vec);
  printArray(vec,(char*) " vec[%d] = %d\n");
  printf(" Pares: %d\n Impares: %d", ret[0], ret[1]);
  return 0;
}
void printArray(int *vec, char *msj){
  int i;
  for(i = 1; i <= vec[0];i++){
    printf(msj, i, vec[i]);
  }
}
void loadRand(int *vec, int limit){
  int i;
  for(i = 1; i <= limit; i++){
    vec[i] = rand();
  }
  vec[0] = i - 1;
}
int * counter(int vec[]){
  int i;
  static int ret[] = {0,0};
  for(i = 1; i <= vec[0]; i++){
    if(vec[i] % 2 == 0){
      ret[0] += 1;
    }
    else{ret[1] += 1;}
  }
  return ret;
}
