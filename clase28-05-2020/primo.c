#include <stdio.h>
#include <stdbool.h>
bool esPrimo(int n);
int main(int argc, char const *argv[]) {
  int val;
  printf(" Ingrese un valor entero: ");
  scanf("%d", &val);
  if(esPrimo(val)){
    printf(" %d es primo.\n", val);
  }
  else{
    printf(" %d no es primo.\n", val);
  }
  return 0;
}
bool esPrimo(int n){
  int c = 0, i = 1;
  while(i <= n){
    if(n % i == 0){c++;}
    i++;
  }
  return c == 2;
}
