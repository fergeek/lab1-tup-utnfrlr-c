#include <stdio.h>
#include <math.h>
#include <string.h>
typedef struct {
  float r;
  float i;
} complex;
typedef struct {
  complex root[99];
  int n;
} complexRoots;
complex sumar(complex z1, complex z2);
complex restar(complex z1, complex z2);
complex conjugado(complex z1);
float modulo(complex z);
void printComplex(complex z);
float argumento(complex z);
complex multiplicar(complex z1, complex z2);
complex dividir(complex z1, complex z2);
complex potencia(complex z, int n);
complex raizk(complex z, int n, int k);
complexRoots raiz(complex z, int n);
int main(int argc, char const *argv[]){
  complex z1, z2, z3; complexRoots roots;
  // printf("Ingrese Z1: ");
  // scanf("%f %fi", &z1.r, &z1.i);
  // printf("Z1 = ");
  z1.r = 1; z1.i = -1;
  z2.r = 3; z2.i = 3;
  z3 = multiplicar(z1,z2);
  roots = raiz(z1,3);
  // printf("Z1 * Z2 = ");
  // printComplex(z3);
  // printf("\nModulo: %.2f", modulo(z3));
  // printf("\nArgumento: %.4f r\n",argumento(z3));
  return 0;
}
complex sumar(complex z1, complex z2){
  complex ret;
  ret.r = z1.r + z2.r;
  ret.i = z1.i + z2.i;
  return ret;
}
complex restar(complex z1, complex z2){
  complex ret;
  ret.r = z1.r - z2.r;
  ret.i = z1.i - z2.i;
  return ret;
}
complex conjugado(complex z1){
  complex ret;
  ret.r = z1.r;
  ret.i = (-1) * z1.i;
  return ret;
}
void printComplex(complex z){
  char str[35] = "";
  strcpy(str, ( (z.r -  trunc(z.r)) == 0 ? "%.2f " : "%.0f "));
  strcat(str, (z.i >= 0 ? "+" : "-"));
  strcat(str, (z.i -  trunc(z.i)) == 0 ? " %.2fi" : " %.0fi");
  printf(str,z.r,z.i * (z.i < 0 ? -1 : 1));
}
float modulo(complex z){
  float ret;
  ret = sqrt(pow(z.r,2)+pow(z.i,2));
  return ret;
}
float argumento(complex z){
  float tmp;
  tmp = acos(z.r/modulo(z));
  return z.i < 0 ? tmp + M_PI : tmp;
}
complex multiplicar(complex z1, complex z2){
  complex ret;
  float p = modulo(z1) * modulo(z2),
  a = atan(z1.r/z1.i) + atan(z2.r/z2.i);
  ret.r = p * cos(a);
  ret.i = p * sin(a);
  return ret;
}
complex dividir(complex z1, complex z2){
  complex ret;
  float p = modulo(z1) / modulo(z2),
  a = atan(z1.r/z1.i) - atan(z2.r/z2.i);
  ret.r = p * cos(a);
  ret.i = p * sin(a);
  return ret;
}
complex potencia(complex z, int n){
  complex ret; float p = pow(modulo(z),n),
  a = (atan(z.r/z.i) * n);
  ret.r = p * cos(a);
  ret.i = p * sin(a);
  return ret;
}
complex raizk(complex z, int n, int k){
  complex ret; float p = modulo(z);
  if(k < n){
    ret.r = p * cos(((atan(z.r/z.i)+(2*k*M_PI)))/n);
    ret.i = p * sin(((atan(z.r/z.i)+(2*k*M_PI)))/n);
  }
  return ret;
}
complexRoots raiz(complex z, int n){
  complexRoots ret; int k;
  for(k = 0; k < n; k++){
    ret.root[k] = raizk(z,n,k);
  }
  ret.n = k;
  return ret;
}
