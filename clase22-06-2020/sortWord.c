#include <stdio.h>
#include <string.h>
#define n 3
#define MAX_CHAR_SIZE 50
int main(int argc, char const *argv[]) {
  char str[MAX_CHAR_SIZE], aux[MAX_CHAR_SIZE], arrayStr[n][MAX_CHAR_SIZE];
  int c = 0, i = 0, j;
  printf(" Ingrese hasta %d palabras separadas por coma: ",n);
  gets(str);
  //ini
  for(j = 0; j < n; j++){arrayStr[j][0] = '\0';}
  //word parser
  while(str[c] != '\0'){
    j = 0;
    while(str[c] != ',' && str[c] != '\0'){
      if(i < n){arrayStr[i][j] = str[c]; c++; j++;}
    }
    arrayStr[i][j] = '\0'; c++; i++;
  }
  //print
  printf(" Input:\n");
  for(i = 0; i < n; i++){
    printf(" * %s\n", arrayStr[i]);
  }
  //sort
  while(c != n-1){
    c = 0;
    for(i = 1; i < n; i++){
        if(strcmp(arrayStr[i],arrayStr[i-1]) < 0){
          strcpy(aux,arrayStr[i]);
          strcpy(arrayStr[i],arrayStr[i-1]);
          strcpy(arrayStr[i-1],aux);
        } else {c++;}
    }
  }
  //print
  printf(" Output:\n");
  for(i = 0; i < n; i++){
    printf(" - %s\n", arrayStr[i]);
  }
  return 0;
}
